const auth = require('../AUTH/auth')
const express = require('express')
const router = express.Router()

router.post('/auth',auth.gerarToken)

module.exports = router;