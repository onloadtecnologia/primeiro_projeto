const controller = require('../CONTROLLER/controller')
const express = require('express')
const router = express.Router()
const auth = require('../AUTH/auth')
const  validacao = require('../VALIDACAO/validacao')

router.get('/',auth.verificarToken,controller.listar)
router.get('/:id',auth.verificarToken,controller.pesquisar)
router.post('/',validacao.validar,auth.verificarToken,controller.salvar)
router.put('/:id',validacao.validar,auth.verificarToken,controller.editar)
router.delete('/:id',auth.verificarToken,controller.deletar)


module.exports = router;
