const jwt = require('jsonwebtoken')
const PRIVATE_KEY = process.env.PRIVATE_KEY

class Auth {

    gerarToken = (req, res) => {
        var { email, password } = req.body
        if (email === "fulano@gmail.com" && password === "fulano123") {

            jwt.sign({ email }, PRIVATE_KEY, { expiresIn: "1h" }, (err, token) => {
                if (err) console.log(err.message)
                res.json({ auth: true, token: token })
            })

        } else {
            res.json({ auth: true, token: token })
        }

    }
    verificarToken = (req, res, next) => {

        
        try {
            
            const token = req.headers['authorization'].split(" ")[1]
            jwt.verify(token, PRIVATE_KEY, (err, decode) => {
                if (err) console.error(`Erro: ${err.message}`)
                req.email = decode.email
                console.log(req.email)
                next()

            })

        } catch (error) {
            res.status(500).json({ auth: false, token: null })

        }

    }
}

module.exports = new Auth()