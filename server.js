const express = require('express')
const app = express()
const cors = require('cors')
const dotenv = require('dotenv').config()
const router = require('./ROUTER/router')
const auth = require('./ROUTER/auth')
const port = process.env.PORT_SERVIDOR || 3000

app.use(cors())
app.use(express.urlencoded({extended:true}))
app.use(express.json())
app.use(express.static("public"))

app.use(auth)
app.use(router)


app.listen(port,(err)=>{
    if(err) console.error(err)

    console.log("http://localhost:"+port)
})