class Validacao {

    validar = (req, res, next) => {
        var { nome, telefone, email, cep } = req.body
        if (nome === undefined || nome === null || nome === "") {
            res.json({ msg: "o campo nome é obrigatório" })
            
        } else if (telefone === undefined || telefone === null || telefone === "") {
            res.json({ msg: "o campo telefone é obrigatório" })

        } else if (email === undefined || email === null || email === "") {
            res.json({ msg: "o campo email é obrigatório" })

        } else if (cep === undefined || cep === null || cep === "") {
            res.json({ msg: "o campo cep é obrigatório" })

        }
        else {
            next()

        }
    }

}

module.exports = new Validacao();