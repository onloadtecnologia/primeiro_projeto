const { Client } = require('pg')
const dotenv = require('dotenv').config()

const client = new Client({
    user: process.env.USER,
    host: process.env.HOST,
    database: process.env.DATABASE,
    password: process.env.PASSWORD,
    port: process.env.PORT_DATABASE
})

client.connect()
    .then(() => {
        client.query(`CREATE TABLE IF NOT EXISTS clientes(
        id SERIAL PRIMARY KEY NOT NULL,
        data VARCHAR(50) NOT NULL,
        nome TEXT NOT NULL,
        telefone VARCHAR(50) NOT NULL,
        email TEXT NOT NULL,
        cep VARCHAR(50) NOT NULL
        )`, (err, res) => {
            if (err) throw err
            console.log(res)

        })

    })
    .catch(err => console.log(err))

module.exports = client;