                    11/12/2021 RIO DE JANEIRO

                            API-REST

DESCRIÇÃO: api base para cadastro de clientes com autenticação jwt, configurada para conexão com banco de dados postgresql.

OPERAÇÕES: LISTAR,PESQUISAR,SALVAR,EDITAR,DELETAR

TECNOLOGIAS: nodejs e banco de dados postgres com a biblioteca pg.

VERSÃO DO NODEJS: 16.5.0

CONFIGURAÇÃO DE ARQUIVO .env
USER=usuario
HOST=host
DATABASE=banco
PASSWORD=senha
PORT_DATABASE=porta
PORT_SERVIDOR=3000
PRIVATE_KEY=chave_secreta

--------------"/auth"--------------
OBJETO DE ENVIO PARA AUTENTICAÇÃO (POST "/auth")
{
    "email":"fulano@gmail.com",
    "password":"meupassword"
}

SUCESSO NA AUTENTICAÇÃO
{
    "auth":true,
    "token":"meutoken"
}

FALHA NA AUTENTICAÇÃO
{
    "auth":false,
    "token":null
}
-------------- "/" ----------------
RETORNO NA REQUISIÇÃO LISTAR (GET "/"):
[
{
"id": 0,
"data": "",
"nome": "",
"telefone": "",
"email": "",
"cep": ""
}
]

RETORNO NA REQUISIÇÃO PESQUISAR (GET '/id')
{
"id": 0,
"data": "",
"nome": "",
"telefone": "",
"email": "",
"cep": ""
}

OBJETO DE ENVIO NA REQUISIÇÃO SALVAR (POST "/"):
{

    "nome": "",
    "telefone": "",
    "email": "",
    "cep": ""

}

PARÂMETRO E OBJETO DE ENVIO NA REQUISIÇÃO EDITAR (PUT "/id"):
{
"nome": "",
"telefone": "",
"email": "",
"cep": ""
}

PARÂMETRO DE ENVIO NA REQUISIÇÃO DELETAR (DELETE "/id"):
{

}
