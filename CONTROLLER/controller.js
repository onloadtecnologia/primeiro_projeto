const client = require('../DB_CONFIG/db_config')

class Controller {

    
    listar = (req, res)=>{
    
        client.query('SELECT * FROM clientes', (err, resp) => {
            if (err) console.error(err)
            res.json(resp.rows).status(200)
        })
      

    }

    pesquisar = (req,res)=>{

        var id = req.params.id;
        client.query('SELECT * FROM  clientes WHERE id=$1',[id],(err, resp) => {
            if (err) console.error(err)
            res.json(resp.rows[0]).status(200)
        })

    }

    salvar = (req,res) => {
       
        var dados={

            data:new Date().toLocaleDateString("pt-BR",{day:'2-digit',month:'2-digit',year:'numeric'}),
            nome:req.body.nome,
            telefone:req.body.telefone,
            email:req.body.email,
            cep:req.body.cep
        } 
        client.query(`INSERT INTO clientes(data,nome,telefone,email,cep) VALUES ($1,$2,$3,$4,$5)`,[
            dados.data,
            dados.nome,
            dados.telefone,
            dados.email,
            dados.cep
        ],(err,resp)=>{

            if(err) console.log(err)
            res.json(resp.rows).status(201) 
        })
        
      


    }

    editar = (req,res) => {
       
        var id = req.params.id
        var dados={

            nome:req.body.nome,
            telefone:req.body.telefone,
            email:req.body.email,
            cep:req.body.cep
        } 
        client.query(`UPDATE clientes SET nome=$1,telefone=$2,email=$3,cep=$4 WHERE id=$5`,[
            dados.nome,
            dados.telefone,
            dados.email,
            dados.cep,
            id


        ],(err,resp)=>{

            if(err) console.log(err)
            res.json(resp.rows).status(201) 
        })
        
      


    }

    deletar = (req,res)=>{

        var id = req.params.id;
        client.query('DELETE FROM clientes WHERE id=$1',[id],(err, resp) => {
            if (err) console.error(err)
            res.json(resp.rows).status(200)
        })

    }






}

module.exports = new Controller()